/*
  DmxSimple - A simple interface to DMX.

  Copyright (c) 2008-2009 Peter Knight, Tinker.it! All rights reserved.
  However, originally released under LGPL by Tinker.it! at:
  https://code.google.com/archive/p/tinkerit/
 
  Copyright (C) 2008-2009 Peter Knight
                2023 Jari Suominen
 
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
  
 */

#ifndef DmxSimple_h
#define DmxSimple_h

#include <inttypes.h>

#if RAMEND <= 0x4FF
#define DMX_SIZE 128
#else
#define DMX_SIZE 512
#endif

class DmxSimpleClass
{
  public:
    void begin();
  	void end();
    void maxChannel(int);
    void write(int, uint8_t);
    void usePin(uint8_t);
    uint8_t* getInternalBuffer();
};
extern DmxSimpleClass DmxSimple;

#endif
